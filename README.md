# Projet-Sin

Projet Sin - License Plate Scanner.

Open explanation : [Here](/explanation/algorigramme_ouverture.png)

# Scanner - Server :

Dependencies:
    
    - Install : NodeJs / Npm 
    - Install : OpenAlpr 
    - Npm : MySQL 

Installation:

    - NodeJS / Npm : `sudo apt install nodejs`
    - Npm package auto-install: `npm i`
    - Npm package manually-install: `npm i mysql`
    
Start Server : `node main.js`

> Enjoy !