const { exec } = require("child_process");
const mysql = require("mysql");
const config = require("./config.json");        // Require Config 
let nbid, lasttype, checkPlate=[], currentImg;  // Variables Declaration

let connection = mysql.createConnection({   // MySQL connection Declaration
    host     : config.mysql.host,
    user     : config.mysql.user,
    password : config.mysql.pass,
    database : config.mysql.dbname
  });

if (config.imgRdm==true){
    currentImg = config.currentImg[Math.floor(Math.random() * config.currentImg.length)];   // Randomize Image if true in config
} else {
    currentImg = config.currentImg[0];
}
console.log(currentImg);    // Send which image he will use (for rdm is better)

exec(`alpr -c ${config.continent} -p ${config.country} ${config.imgPath+currentImg}`, (error, stdout, stderr) => {  // Execute alpr command to scan image
    if (stdout != "No license plates found.\n") {
        // console.log("stdout : " + stdout)
        alprTreatment(stdout);  // if Plate exist on image => alprTreatment();

    } else {
        console.log(config.message.noPlateFound);   // Plate don't exist send message in config
    }
});

function alprTreatment(result){     // Plate exist on image
    // console.log(stdout, stderr, error);
    checkPlate=[];
    for(i=1;i<=3;i++){  // Check the three first plate
        // console.log("Result " + result);
        let firstres = result.split("\n");  // Parsing String to array for analyse
        let items = firstres[i].split("\t ");
        let plate = [{"plate":items[0].trim().slice(2)}, {"matchPercentage":items[1].trim().slice(12)}, {"useless":items[2].trim().slice(15)}];
        // console.log(plate[0].plate);

        connection.query("SELECT COUNT(*) FROM passages;", function (error, results, fields) {  // Check number of "id" in "passages" to add 1
            // console.log(results[0]);
            // "+`"${immatsId}","${meaning}",${last_visit}`+"
            nbraw = JSON.parse(JSON.stringify(results))[0]
            nbid = nbraw['COUNT(*)'] + 1;
            // console.log(`NbID : ${nbid} ; Nbraw : ${nbraw['COUNT(*)']}`);
        });

        dbCheck(plate, (res)=>{     // Execute => dbCheck();

            /* dbCheck response (callback) : */
            let json = JSON.parse(JSON.stringify(res))[0]       // Transforming Array to Array Usable
            if(JSON.stringify(res)!="[]" && json.allowed == "true"){    // Check if response != "[]" or if the people not allowed to enter
                // console.log(res);
                lastPass = new Date().getTime().toString();     // Setting the time to a variable for change last visit in db
                dbWriter(lastPass.slice(0,10), json.id, config.simulationType[Math.floor(Math.random() * config.simulationType.length)],json.last_type, (result, error)=>{ // Execute dbWriter(); => Interact with the database
                    console.log(result);    // Send the result, if people his inside or allowed to pass
                });
                connection.end();   // Ending connection with DataBase for security
            } else if (JSON.stringify(checkPlate)=="[[],[]]" && (JSON.stringify(res)=="[]" || json.allowed == "false")) {
                console.log(config.message.unAuthorized); // Send UnAuthorized if plate not in db or not reconized or if people is not allowed
                connection.end();   // Ending connection with DataBase for security
            } else if (JSON.stringify(res)=="[]" || json.allowed == "false"){
                checkPlate.push([]);
                // console.log(JSON.stringify(checkPlate));
            } else {
                console.log("Error");
                connection.end();   // Ending connection with DataBase for security
            }
            /**********************************/
        })
    }
}

function dbCheck(res, callback){    // Check if plate exist in db and return the row or "[]" if don't exist.
    // console.log(res[0].plate);
    connection.query(`SELECT * FROM ${config.mysql.table} WHERE ${config.mysql.plateColumn} = "` + res[0].plate + '"', function (error, results, fields) {
        // console.log(results[0]);
        callback(results);
    });
}

/* 

       /!\ This function interact with the DataBase, Please be careful if you modify it !  /!\ :

*/

function dbWriter(last_visit,immatsId,meaning,last_type, callback){
    // console.log(immatsId, meaning, last_type);
    if(meaning=="entree" && last_type=="sortie"){       // "meaning" = "sens", "là où il veut aller"
        lasttype = "entree";                            // "last_type" = "dernier endroit", "là où il est"
        ok();                                           // Execute ok(); if condition is true;  ok(); => "Change Database value and add row in passages"
        callback(config.message.authorized);
    } else if(meaning=="sortie") {
        lasttype = "sortie";
        ok();
        callback(config.message.authorized);            
    } else {
        callback(config.message.alreadyInside);         // if people is already inside and try to enter then display a message (in config)
    }
    
    function ok(){      
        connection.query(`UPDATE ${config.mysql.table} SET last_visit = `+ `'${last_visit}' WHERE id = '${immatsId}'`, function (error, results, fields) {
            // console.log(results[0]);
            // console.log(results, error)
        });                                             // Change the last visit date to now (timestamp)


        connection.query(`UPDATE ${config.mysql.table} SET ${config.mysql.blockingEntryColumn} = `+ `'${lasttype}' WHERE id = '${immatsId}'`, function (error, results, fields) {
            // console.log(results[0]);
            // "+`"${immatsId}","${meaning}",${last_visit}`+"
            // console.log("INSERT INTO `passages` VALUES ("+`${nbid},"${immatsId}","${meaning}",${last_visit}`+");")
            // console.log(results, error)
        });                                             // Change if people is inside or outside
        
        // console.log(nbid);
        connection.query("INSERT INTO `passages` VALUES ("+`"${nbid}","${immatsId}","${meaning}",${last_visit}`+");", function (error, results, fields) {
            // console.log(results);
            // "+`"${immatsId}","${meaning}",${last_visit}`+"
            // console.log("INSERT INTO `passages` VALUES ("+`${nbid},"${immatsId}","${meaning}",${last_visit}`+");")
            // console.log(results, error)
        });                                             // Create a Row in passages for history
    }
    
}